$(document).ready(function(){
    var resultData = sotrefile;
	displayStoreDetails(resultData);
	$(".nav-tabs a").click(function(){	  	
	    $(this).tab('show');
	});	 
	$(function(){
		var options = {
			placement: function (context, element) {
				var position = $(element).position();
				console.log(position.top - $(window).scrollTop());
				if (position.left < 280) {
				     return "right";
				}
				
				if (position.top < 280){
					return "bottom";
				}
				            
				else{
				     return "left";
				}
			}, trigger: "hover"
		};
		$(".test").tooltip(options);
	});
});

function array_search(val, array) {
  if(typeof(array) === 'array' || typeof(array) === 'object') {
    var rekey;
    for(var indice in array) {    	
      if(indice == val) {
        rekey = array[indice];
        break;
      }
    }
    return rekey;
  }
}	

var dayArr = {'mon':'Monday','tue':'Tuesday','wed':'Wednesday','thu':'Thursday','fri':'Friday','sat':'Saturday','sun':'Sunday'};

function displayStoreDetails(resultData){

	$("#storeName").html("Welcome to "+resultData.storeName);  		
	var addressVal = resultData.address;
	var telephone = resultData.telephone;
	var fullAddress = addressVal.addressLine1+"<br/>"+addressVal.addressLine2+"<br/>"+addressVal.county+"<br/>"+addressVal.city+" , "+addressVal.postcode+"<br/>"+telephone;
	$("#storeAddress").html(fullAddress);
	var depList = resultData.departments;
	var moreServicesData = resultData.services;
	var locationData = resultData.location;
	var satnavData = resultData.satnav;
	var latitude = locationData.latitude;
	var longitude = locationData.longitude;
	var satnavlatitude = satnavData.latitude;
	var satnavlongitude = satnavData.longitude;
	$("#locLat").html(latitude);
	$("#locLong").html(longitude);
	$("#satLat").html(satnavlatitude);
	$("#satLong").html(satnavlongitude);
	var i = 0;	
	var openTime = '';
	var defultOpeningTime = resultData.openingTimes;
	var linkedStoreData = resultData.linkedLocations;
	var tabData = new Array();
	var tabservice = new Array();
	tabservice.push('Store');
	tabData.push(defultOpeningTime);
	var whatInStore = '';
	var moreServices = '';
	$.each(depList, function (key, val) {			
		var serviceName = val.serviceName;
		var service = val.name;
		var className = '';
			if(i==0){
				className =	'class="active"';
				i++;	
			}
		var serviceImg = '';
		var toolTipData = '';
			if(val.openingTimes){	
				var depOpen = val.openingTimes;
				tabservice.push(serviceName);
				tabData.push(depOpen);			 	
			 	if(service=="cafe"){
			  		serviceImg = 'icon_cafe.gif';
			  		toolTipData = 'If you fancy a relaxing bite to eat, our café serves freshly prepared meals to your table, from all-day breakfasts to hot puddings with custard as well as freshly ground coffee.';
			  	}			
				else if(service=="pharmacy"){
			  		serviceImg = 'icon_pharmacy.gif';
			  		toolTipData = 'Get free advice on medicines and healthcare from our pharmacists - no appointment necessary - and why not have your prescriptions dispensed while you shop.';
			  	}	
			  	moreServices += '<li><p class = "tooltip-options"><a href = "#" data-toggle = "tooltip"  title = "'+toolTipData+'" class="test"><img src="assets/image/'+serviceImg+'" alt="'+serviceName+'"></a></p></li>';			  
			}
			else{

				if(service=="bakery"){
				  	serviceImg = 'icon_familybaker.gif';
				  	toolTipData = 'Our trained bakers bake bread throughout the day – 127 different kinds of bread, to be precise.';
				}			
			  	else if(service=="butcher"){
			  		serviceImg = 'icon_familybutcher.gif';
			  		toolTipData = 'Our trained butchers cut and prepare 100% fresh British beef, pork and lamb in-store, offering you a larger selection of cuts than any other supermarket.';
			  	}	
			    else if(service=="provisions"){
			  		serviceImg = 'icon_provisions.gif';
			  		toolTipData = 'Ask for a little taste before you buy, and we’ll cut and weigh anything from our range of delicacies, including cooked meat, olives and cheese.';
			    }	
			    else if(service=="clothing"){
			  		serviceImg = 'icon_nutmeg.gif';
			  		toolTipData = 'Nutmeg is a clothing brand full of thoughtful details. Little things that make everything a little easier, a little brighter and a lot more comfortable..';
			    }	
			    else if(service=="ovenFresh"){
			  		serviceImg = 'icon_ovenfresh.gif';
			  		toolTipData = 'Enjoy all your favourite sweet and savoury pies, and comfort food like roast chicken, and ribs – straight from the oven.';
			    }	
			    else if(service=="saladBar"){
			  		serviceImg = 'icon_saladbar.gif';
			  		toolTipData = 'Grab one of our pizzas or salads, freshly prepared in store!';
			    }	
			    else if(service=="fishmonger"){
			  		serviceImg = 'icon_fishmonger.gif';
			  		toolTipData = 'Our trained fishmongers are available all day to prepare any of our fish, plus advise on the best buys and preparation ideas.';
			  	}	
			  	else if(service=="cakeShop"){
			  		serviceImg = 'icon_cakeshop.gif';
			  		toolTipData = 'Many of our tempting cakes and desserts – pastries, tarts, doughnuts and gateaux - are prepared by hand in store throughout the day.';
			  	}	
			    else {
			  		serviceImg = 'icon_familybutcher.gif';
			  		toolTipData = 'Our trained bakers bake bread throughout the day – 127 different kinds of bread, to be precise.';
			    }
				whatInStore += '<li><p class = "tooltip-options"><a href = "#" data-toggle = "tooltip"  title = "'+toolTipData+'" class="test"><img src="assets/image/'+serviceImg+'" alt="'+serviceName+'"></a></p></li>';
			}
	});

	$.each(linkedStoreData, function (key, val) {
		var linkServiceName = val.category;
		var linkServiceOpening = val.openingTimes;		
		tabservice.push(linkServiceName);
		tabData.push(linkServiceOpening);
	});

	
	$("#whatInStore").html(whatInStore); 
	displayStoreOpen(tabservice,tabData);
	$.each(moreServicesData, function (key, val) {	
		var moreService = val.name;
		var moreServiceName = val.serviceName;
		var serviceImg = '';
		var toolTipData = '';
		if(moreService=="freeParking"){
			serviceImg = 'icon_familybaker.gif';
			toolTipData = 'Our trained bakers bake bread throughout the day – 127 different kinds of bread, to be precise.';
		}			
		else if(moreService=="disabledFacilities"){
			serviceImg = 'icon_disabledfacilities.gif';
			toolTipData = 'We offer lots of services to our shoppers with additional needs, including dedicated parking, wheelchairs, staff assistance, seating and induction loops.';
		}	
		else if(moreService=="recycling"){
			serviceImg = 'icon_recyclingcenter.gif';
			toolTipData = 'Recycling.';
		}	
		else if(moreService=="parentAndChild"){
			serviceImg = 'icon_parent.gif';
			toolTipData = '>We offer convenient parking for parents with small children, making it easier to manage prams and trolleys.';
		}	
		else if(moreService=="nationalLottery"){
			serviceImg = 'icon_tnl.gif';
			toolTipData = 'Get your ticket at the Lottery desk in store.';
		}	
		else if(moreService=="24HourCash"){
			serviceImg = 'icon_cash.gif';
			toolTipData = 'Get cash any time at the convenient ATMs outside our store.';
		}	
		else if(moreService=="calorGas"){
			serviceImg = 'icon_calor.gif';
			toolTipData = 'Calor Gas LPG cylinders can be used for patio heaters, barbeques, caravanning and torches.';
		}	
		else if(moreService=="carpetCleaning"){
			serviceImg = 'icon_carpetcleaning.gif';
			toolTipData = "If your carpets could do with a clean or you're thinking about a new carpet, rent a Rug Doctor first - you'll be amazed at the results!";
		}	
		else if(moreService=="carwash"){
			serviceImg = 'icon_petrol.gif';
			toolTipData = 'Stop by our petrol station to fill up and enjoy many other services like gas, a car wash and snacks for the road.';
		}	
		else if(moreService=="lpg"){
			serviceImg = 'icon_lpg.gif';
			toolTipData = 'We stock LPG autogas, another cost effective and environmentally-friendly fuel alternative to petrol and diesel.';
		}	
		else if(moreService=="dryCleaning"){
			serviceImg = 'icon_cakeshop.gif';
			toolTipData = 'Many of our tempting cakes and desserts – pastries, tarts, doughnuts and gateaux - are prepared by hand in store throughout the day.';
		}	
		else if(moreService=="photoPrinting"){
			serviceImg = 'icon_photoprocessing.gif';
			toolTipData = 'We offer quality digital photo printing in-store, with 1 hour services available.';
		}	
		else {
			serviceImg = 'icon_familybutcher.gif';
			toolTipData = 'Our trained bakers bake bread throughout the day – 127 different kinds of bread, to be precise.';
		}	
				  
		moreServices += '<li><p class = "tooltip-options"><a href = "#" data-toggle = "tooltip"  title = "'+toolTipData+'" class="test"><img src="assets/image/'+serviceImg+'" alt="'+moreServiceName+'"></a></p></li>';
	});
		
	$("#moreServicesId").html(moreServices);

}

function displayStoreOpen(tabList,defultOpeningTimes){
	var serviceList = '<ul class="nav nav-tabs">';
	var tabId = 0;
	$.each(tabList, function (key, tabName) {	
		var liClass = '';
		if(tabId==0) {
			liClass = 'class="active"';
		}
		serviceList +='<li '+liClass+'><a href="#'+tabName+'">'+tabName+'</a></li>';
		tabId = 1;
	});
	serviceList +="</ul>"
	$("#serviceList").html(serviceList);
	var dataDiv = '';
	var i=0;
	$.each(defultOpeningTimes, function (key, defultOpeningTime) {
		
		if(defultOpeningTime){			
			var tabClass = '';
			if(i==0){
				tabClass = 'active';
			}
			var openTime = '';
			openTime = '<div id="'+tabList[key]+'" class="tab-pane  fade in '+tabClass+'">';
			openTime += '<div class="table-responsive">';	
			openTime += '<table class="table">';	
			openTime += '<caption class="tbl-caption">Opening times</caption> ';	
			openTime += '<thead>';	
			openTime += '<tr>';	
			var openAndClose = new Array();
			$.each(defultOpeningTime, function (key, val) {	
				var openingTimeArr = val.open.split(':');
				var openingTime = openingTimeArr[0]+openingTimeArr[2];
				var closeTimeArr = val.close.split(':');
				var closeTime = closeTimeArr[0]+closeTimeArr[1];
				var dayDisplay = array_search(key, dayArr);
				openTime += '<th scope="col">'+dayDisplay+'</th>';	
				openAndClose.push(openingTime+' - '+closeTime);
			});	
			openTime += '</tr>';	
			openTime += '</thead>';	
			openTime += '<tbody>';	
			openTime += '<tr>';	
			var d = new Date();
			var n = d.getDay();
			if(n==0){
				n=6;
			}
			else{
				n = n - 1;
			}			
			$.each(openAndClose, function (key, val) {	
				var dayClass = '';
				if(n==key){
					dayClass = 'class="selectedDay"';
				}
				openTime += '<td '+dayClass+'>'+val+'</td>';	
			});
			openTime += '</tr>';
			openTime += '</tbody>';
			openTime += '</table>';	
			openTime += '</div>';	
			openTime += '</div>';	
			dataDiv += openTime;
			i++;			
		}
				
	});
	$("#openingTime").html(dataDiv);
}