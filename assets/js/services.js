'use strict';

/* Services */

var storeServices = angular.module('storeServices', ['ngResource']);

storeServices.factory('Store', ['$resource',
  function($resource){      
    return $resource("https://cit-api.morrisons.com/location/v2/:dest/:storeId/:openingtime", {},{
            'getStoreAll': {method: 'GET', isArray: false } ,
            'getStore': {method: 'GET', isArray: false }, 
            'getStoreOpening': {method: 'GET', isArray: true } 
           });
}]);

storeServices.value('imagePath', function (service) {
    var images = null;
    if (service == "nationalLottery") {
        images = 'assets/image/icon_tnl.gif'
    }
    else if (service == "cafe") {
        images = 'assets/image/icon_cafe.gif';
    }
    else if (service == "pharmacy") {
        images = 'assets/image/icon_pharmacy.gif';
    }
    else if (service == "bakery") {
        images = 'assets/image/icon_familybaker.gif';
    }
    else if (service == "butcher") {
        images = 'assets/image/icon_familybutcher.gif';
    }
    else if (service == "provisions") {
        images = 'assets/image/icon_provisions.gif';
    }
    else if (service == "nutmeg") {
        images = 'assets/image/icon_nutmeg.gif';
    }
    else if (service == "ovenFresh") {
        images = 'assets/image/icon_ovenfresh.gif';
    }
    else if (service == "saladBar") {
        images = 'assets/image/icon_saladbar.gif';
    }
    else if (service == "fishmonger") {
        images = 'assets/image/icon_fishmonger.gif';
    }
    else if (service == "theCakeShop") {
        images = 'assets/image/icon_cakeshop.gif';
    }
    else if (service == "freeParking") {
        //      images = 'assets/image/icon_familybaker.gif';
        images = 'assets/image/icon_parking.gif';
    }
    else if (service == "disabledFacilities") {
        images = 'assets/image/icon_disabledfacilities.gif';
    }
    else if (service == "recycling") {
        images = 'assets/image/icon_recyclingcenter.gif';
    }
    else if (service == "parentAndChild") {
        images = 'assets/image/icon_parent.gif';
    }
    else if (service == "24HourCash") {
        images = 'assets/image/icon_cash.gif';
    }
    else if (service == "calorGas") {
        images = 'assets/image/icon_calor.gif';
    }
    else if (service == "carpetCleaning") {
        images = 'assets/image/icon_carpetcleaning.gif';
    }
    else if (service == "carwash") {
        //      images = 'assets/image/icon_petrol.gif';
        images = 'assets/image/icon_carwash.gif';
    }
    else if (service == "lpg") {
        images = 'assets/image/icon_lpg.gif';
    }
    else if (service == "dryCleaning") {
        //      images = 'assets/image/icon_cakeshop.gif';
        images = 'assets/image/icon_drycleaning.gif';
    }
    else if (service == "photoPrinting") {
        images = 'assets/image/icon_photoprocessing.gif';
    }
    //    else if(service=="photoPrinting"){
    else if (service == "matchAndMore") {
        images = 'assets/image/icon_Match_and_More.gif';
    }
    else if (service == "gardenCentre") {
        images = "assets/image/icon_gardencentre.gif";
    }

    return images;
});

storeServices.value('toolTipData', function (service) {
    var toolTipData = null;
    if (service == "nationalLottery") {
        toolTipData = 'Get your ticket at the Lottery desk in store.';
    }
    else if (service == "cafe") {
        toolTipData = 'If you fancy a relaxing bite to eat, our cafe serves freshly prepared meals to your table, from all-day breakfasts to hot puddings with custard as well as freshly ground coffee.';
    }
    else if (service == "pharmacy") {
        toolTipData = 'Get free advice on medicines and healthcare from our pharmacists - no appointment necessary - and why not have your prescriptions dispensed while you shop.';
    }
    if (service == "bakery") {
        toolTipData = 'Our trained bakers bake bread throughout the day – 127 different kinds of bread, to be precise.';
    }
    else if (service == "butcher") {
        toolTipData = 'Our trained butchers cut and prepare 100% fresh British beef, pork and lamb in-store, offering you a larger selection of cuts than any other supermarket.';
    }
    else if (service == "provisions") {
        toolTipData = 'Ask for a little taste before you buy, and we’ll cut and weigh anything from our range of delicacies, including cooked meat, olives and cheese.';
    }
    //    else if(service=="clothing"){
    else if (service == "nutmeg") {
        toolTipData = 'Nutmeg is a clothing brand full of thoughtful details. Little things that make everything a little easier, a little brighter and a lot more comfortable..';
    }
    else if (service == "ovenFresh") {
        toolTipData = 'Enjoy all your favourite sweet and savoury pies, and comfort food like roast chicken, and ribs – straight from the oven.';
    }
    else if (service == "saladBar") {
        toolTipData = 'Grab one of our pizzas or salads, freshly prepared in store!';
    }
    else if (service == "fishmonger") {
        toolTipData = 'Our trained fishmongers are available all day to prepare any of our fish, plus advise on the best buys and preparation ideas.';
    }
    else if (service == "theCakeShop") {
        toolTipData = 'Many of our tempting cakes and desserts – pastries, tarts, doughnuts and gateaux - are prepared by hand in store throughout the day.';
    }
    else if (service == "freeParking") {
        //        toolTipData = 'Our trained bakers bake bread throughout the day – 127 different kinds of bread, to be precise.';
        toolTipData = 'Enjoy Free Parking at this store.';
    }
    else if (service == "disabledFacilities") {
        toolTipData = 'We offer lots of services to our shoppers with additional needs, including dedicated parking, wheelchairs, staff assistance, seating and induction loops.';
    }
    else if (service == "recycling") {
        toolTipData = 'Recycling.';
    }
    else if (service == "parentAndChild") {
        toolTipData = 'We offer convenient parking for parents with small children, making it easier to manage prams and trolleys.';
    }
    else if (service == "nationalLottery") {
        toolTipData = 'Get your ticket at the Lottery desk in store.';
    }
    else if (service == "24HourCash") {
        toolTipData = 'Get cash any time at the convenient ATMs outside our store.';
    }
    else if (service == "calorGas") {
        toolTipData = 'Calor Gas LPG cylinders can be used for patio heaters, barbeques, caravanning and torches.';
    }
    else if (service == "carpetCleaning") {
        toolTipData = "If your carpets could do with a clean or you're thinking about a new carpet, rent a Rug Doctor first - you'll be amazed at the results!";
    }
    else if (service == "carwash") {
        toolTipData = 'Stop by our petrol station to fill up and enjoy many other services like gas, a car wash and snacks for the road.';
    }
    else if (service == "lpg") {
        toolTipData = 'We stock LPG autogas, another cost effective and environmentally-friendly fuel alternative to petrol and diesel.';
    }
    else if (service == "dryCleaning") {
        //        toolTipData = 'Many of our tempting cakes and desserts – pastries, tarts, doughnuts and gateaux - are prepared by hand in store throughout the day.';
        toolTipData = 'Dry Cleaning';
    }
    else if (service == "photoPrinting") {
        toolTipData = 'We offer quality digital photo printing in-store, with 1 hour services available.';
    }
    else if(service == "gardenCentre") {
        //        toolTipData = 'Our trained bakers bake bread throughout the day – 127 different kinds of bread, to be precise.';
        toolTipData = "Plan your perfect garden by visiting the Garden Centre when you're next in store";
    }

    return toolTipData;
});
