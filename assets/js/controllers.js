'use strict';

/* Controllers */

var storeControllers = angular.module('storeControllers', ['ui.bootstrap']);

storeControllers.controller('StoreFinderCtrl', ['$scope', 'Store' ,'$http', 'Bing', 'myCache',
  function($scope, Store, $http, Bing, myCache) {     

    var cacheStoreRS = myCache.get('result');
    var cacheStorePoint = myCache.get('cacheLatLog');

    if (cacheStoreRS) {     

      $('#storeSecificService').removeClass("show").addClass("hide");  
      $('#storeCityListingDiv').removeClass("show").addClass("hide");       
      $('#storeListingDiv').addClass("show"); 
      $scope.dbMarkers = cacheStoreRS;           
      var latlonArray = cacheStorePoint.split('::');
      var searchLat_scope = latlonArray[0];
      var searchLon_scope = latlonArray[1];
      GetMap(searchLat_scope,searchLon_scope,cacheStoreRS);

    }
    
    $scope.findStore = function(){

      if ($scope.storeForm.$valid) {     

          var selectedStore_scope = $scope.selectedArea;  

          Bing.get($scope.searchTerm).success(function (response) {
            myCache.remove('result');
            myCache.remove('cacheLatLog');
            var dbSelectedCityList = response.resourceSets[0].resources;
            var selectedCity_scope = [];
              for (var i = 0, l = response.resourceSets[0].resources.length; i < l; i++) {
                var obj = {"city":dbSelectedCityList[i].name,"cityLatlong":dbSelectedCityList[i].point.coordinates[0]+"::"+dbSelectedCityList[i].point.coordinates[1]};
                selectedCity_scope.push(obj);                        
              }
            
              if(response.resourceSets[0].resources.length == 1){
                $('#storeSecificService').removeClass("show").addClass("hide"); 
                  $scope.viewStoreList(dbSelectedCityList[0].point.coordinates[0]+"::"+dbSelectedCityList[0].point.coordinates[1]);
                }
                else{
                  $scope.dbCityList = selectedCity_scope;                
                  $('#storeSecificService').removeClass("show").addClass("hide");  
                  $('#storeListingDiv').removeClass("show").addClass("hide");       
                  $('#storeCityListingDiv').addClass("show");
                  
                }
        })
        .error( function (response) {
              console.log(response);
        });                    
      }
  }

  $scope.viewStoreList = function(latlog){
    var latlonArray = latlog.split('::');
    var searchLat_scope = latlonArray[0];
    var searchLon_scope = latlonArray[1];

    if(searchLat_scope && searchLon_scope){       
        var data = Store.getStoreAll({dest:'stores',apikey:'Avdsv8PKMF8tjAkYJYYl1QE4SZBlYJKJ',distance:50000,lat:encodeURIComponent(searchLat_scope),limit:10,lon:encodeURIComponent(searchLon_scope),offset:0,storeformat:'supermarket'}).$promise.then(function (success) {
            var dbMarkers_scope = [];            
                    
            for (var i = 0, l = success.stores.length; i < l; i++) {
                var obj = {"id":success.stores[i].name,"storeName":success.stores[i].storeName,"latitude":success.stores[i].location.latitude,"longitude":success.stores[i].location.longitude,"distance":success.stores[i].distance,"telephone":success.stores[i].telephone,"postcode":success.stores[i].address.postcode,"city":success.stores[i].address.city,"county":success.stores[i].address.county,"addressLine1":success.stores[i].address.addressLine1,"addressLine2": success.stores[i].address.addressLine2};
                dbMarkers_scope.push(obj);                        
            }
                    
            $scope.dbMarkers = dbMarkers_scope;
            myCache.put('result', dbMarkers_scope);
            myCache.put('cacheLatLog', latlog);
            GetMap(searchLat_scope,searchLon_scope,dbMarkers_scope);
        }, function (error) {
              alert("error");
        });
                  
        $('#storeCityListingDiv').removeClass("show").addClass("hide");       
        $('#storeListingDiv').addClass("show");       
    }
  }
}]);



storeControllers.controller('StoreDetailCtrl', ['$scope', '$routeParams', '$sce', 'imagePath', 'toolTipData', 'Store',
  function($scope, $routeParams, $sce, imagePath, toolTipData , Store) {   
    var data = Store.getStore({dest:'stores',storeId:$routeParams.storeId,apikey:'Avdsv8PKMF8tjAkYJYYl1QE4SZBlYJKJ',include:'departments,services,linkedStores'}).$promise.then(function (storeData) {
      var selectedStore_scope = $scope.selectedStore;  
      var services_scope = $scope.services;   
      var depart_scope = $scope.depart_scope;   
      var linkedLocations_scope = $scope.linkedLocations_scope;     
      var tabData = new Array();   
      selectedStore_scope = storeData;
      selectedStore_scope.name = storeData.name;
      selectedStore_scope.storeName = storeData.storeName;
      selectedStore_scope.storeFormat = storeData.storeFormat;
      selectedStore_scope.category = storeData.category;
      selectedStore_scope.region = storeData.region;
      selectedStore_scope.telephone = storeData.telephone;
      selectedStore_scope.address.addressLine1 = storeData.address.addressLine1;
      selectedStore_scope.address.addressLine2 = storeData.address.addressLine2;
      selectedStore_scope.address.county = storeData.address.county;
      selectedStore_scope.address.city = storeData.address.city;
      selectedStore_scope.address.postcode = storeData.address.postcode;
      selectedStore_scope.address.country = storeData.address.country;
      selectedStore_scope.location.latitude = storeData.location.latitude;
      selectedStore_scope.location.longitude = storeData.location.longitude;
      selectedStore_scope.satnav.latitude = storeData.satnav.latitude;
      selectedStore_scope.satnav.longitude = storeData.satnav.longitude;
      var defultOpeningTime = storeData.openingTimes;
      var tabservice = new Array();
      tabservice.push('Store');
      tabData.push(defultOpeningTime);

      /* Getting the store services */
      services_scope = [];
        for (var i = 0, l = storeData.services.length; i < l; i++) {                  
          var obj = {"serviceName":storeData.services[i].serviceName,"displayImage":$sce.trustAsResourceUrl(imagePath(storeData.services[i].name)),"toolTipe":toolTipData(storeData.services[i].name)};
          services_scope.push(obj);  
        }

      /* Getting the linked location */
      linkedLocations_scope = [];
        for (var i = 0, l = storeData.linkedLocations.length; i < l; i++) {
             var  storeNames = "";
             if(storeData.linkedLocations[i].storeFormat=="pfs"){
                storeNames ="petrol station";
                
             }
             else{
                storeNames = storeData.linkedLocations[i].serviceName;

             }
             var obj = {"serviceName":storeNames,"displayImage":$sce.trustAsResourceUrl(imagePath(storeData.linkedLocations[i].name)),"toolTipe":toolTipData(storeData.linkedLocations[i].name)};
             linkedLocations_scope.push(obj);
               if(storeData.linkedLocations[i].openingTimes){
                tabservice.push(storeNames);
                tabData.push(storeData.linkedLocations[i].openingTimes);
               }
        }
        
      depart_scope = [];
        for (var i = 0, l = storeData.departments.length; i < l; i++) {
            var obj = {"serviceName":storeData.departments[i].serviceName,"displayImage":$sce.trustAsResourceUrl(imagePath(storeData.departments[i].name)),"toolTipe":toolTipData(storeData.departments[i].name)};
            depart_scope.push(obj);
              if(storeData.departments[i].openingTimes){
                tabservice.push(storeData.departments[i].serviceName);
                tabData.push(storeData.departments[i].openingTimes);
              }
        }        

        var storeOpeningTimes_scope = $scope.storeOpeningTimes;
        $scope.selectedStore = selectedStore_scope;        
        $scope.services = services_scope;
        $scope.department = depart_scope;
        var tabContent = { };
          $.each(tabservice, function (tabNkey, tabNval) { 
              var openAndClose = new Array();
                $.each(tabData[tabNkey], function (tabkey, val) { 
                    var openingTimeArr = val.open.split(':');
                    var openingTime = openingTimeArr[0]+openingTimeArr[2];
                    var closeTimeArr = val.close.split(':');
                    var closeTime = closeTimeArr[0]+closeTimeArr[1];
                    openAndClose.push(openingTime+' - '+closeTime);
              }); 
              tabContent[tabNval] = openAndClose;
            });

            $scope.storeOpeningTimes = storeOpeningTimes_scope;
            $scope.tabservice = tabservice;
            $scope.tabContent = tabContent;
        }, 
          function (error) {
              alert("error");

    });
    
}]);

