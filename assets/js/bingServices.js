'use strict';

/* Services */

var bingServices = angular.module('bingServices', []);

bingServices.factory('Bing', ['$http',
  function($http){  	  	
    return {
        get: function (searchTerm) {        	
        	var url = "http://dev.virtualearth.net/REST/v1/Locations?query="+encodeURIComponent(searchTerm)+",UK&output=json&jsonp=JSON_CALLBACK&key=AhoE-XgzmBlxWBzP1DpGPhnlgZWqKiqEwO4EI3BVLlym8VOPZbS7qBieq4YBwtwQ"
            return $http.jsonp( url )
            .success( function (data) {            	
            	return data;
            	
            })
            .error( function (data) {
              console.log(data);
            });
        }
    };
}]);