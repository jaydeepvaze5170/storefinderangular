'use strict';

/* App Module */

var storeApp = angular.module('storeApp', [
  'ngRoute',
  'storeControllers',
  'storeServices',
  'bingServices'
]);

storeApp.config(['$routeProvider','$locationProvider',
  function($routeProvider, $locationProvider) {
    $routeProvider.
      when('/stores', {
        templateUrl: 'partials/storeFinder.html',
        controller: 'StoreFinderCtrl'
      }).
      when('/stores/:storeId', {
        templateUrl: 'partials/storeDetail.html',
        controller: 'StoreDetailCtrl'
      }).
      otherwise({
        redirectTo: '/stores'
      });

      $locationProvider.html5Mode(true);
  }
]);

storeApp.factory( 'myCache', function($cacheFactory) {
  var myCache = $cacheFactory('myCache');
  return myCache;
});
